## JSON-API

* **Получить список всех кошельков отсортированных по полю currency_id:**

> Route: /currencies?sort=currency_id

```
GET /currencies?sort=currency_id HTTP/1.1
```   


* **Получить список кошельков с фильтрацией по полю deleted со значением 0:**

> Route: /currencies?filter[deleted]=0

```
GET /currencies?filter[deleted]=0 HTTP/1.1
```   


* **Получить кошелек с id=1 включая релативные данные: currency:**

> Route: /wallet/1?include=currency

```
GET /wallet/1?include=currency HTTP/1.1
```   
   
   
* **Получить список всех кошельков, но с ограниченным набором полей: wallet_id, amount:**

> Route: /wallets?fields=wallet_id,amount

```
GET /wallets?fields[wallets]=wallet_id,amount HTTP/1.1
```