## JSON-RPC

* **Получения списка всех е-валют:**

```json
{
    "jsonrpc": "2.0", 
    "method": "getCurrencies", 
    "id": 1
}
```

* **Обновления полей short_name и actual_course для е-валюты с id=4:**

```json
{
    "jsonrpc": "2.0", 
    "method": "updateCurrency", 
    "params": {
        "id": 4,
        "short_name": "new name", 
        "actual_course": 600
    }, 
    "id": 2
}
```

* **Удаления е-валюты с id=3:**

```json
{
    "jsonrpc": "2.0", 
    "method": "deleteCurrency", 
    "params": {
        "id": 3
    }, 
    "id": 3
}
```

* **Получения данных о е-валюте с id=5:**

```json
{
    "jsonrpc": "2.0", 
    "method": "getCurrency", 
    "params": {
        "id": 5
    },
    "id": 4
}
```